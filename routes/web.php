<?php

use App\Http\Controllers\CourseController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegistrationController;
use Illuminate\Support\Facades\Route;


Route::resource('courses', CourseController::class)->except([
    'show',
]);

Route::get('courses/api', [CourseController::class, 'api'])->name('courses.api');
Route::get('/', function(){
    return view('auth.login');
});
Route::get('login_show',[LoginController::class, 'login_show'])->name('login_show');
Route::post('login',[LoginController::class, 'login'])->name('login');
Route::get('registration_show',[LoginController::class,'registration_show'])->name('registration_show');
Route::post('registration',[LoginController::class,'registration'])->name('registration');
//
//Route::get('logout',[LoginController::class,'logout'])->name('logout');


