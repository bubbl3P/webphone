<!-- ========== Left Sidebar Start ========== -->
<div class="left-side-menu">


        <!--- Sidemenu -->
        <ul class="metismenu side-nav">

            <li class="side-nav-title side-nav-item">Menu</li>

            <li class="side-nav-item">
                <a href="javascript: void(0);" class="side-nav-link">
                    <i class="uil-home-alt"></i>
                    <span class="badge badge-success float-right">5</span>
                    <span> Dashboards </span>
                </a>
                <ul class="side-nav-second-level" aria-expanded="false">
                    <li>
                        <a href="#">Employee management</a>
                    </li>
                    <li>
                        <a href="#">Product Management</a>
                    </li>
                    <li>
                        <a href="#">Order management</a>
                    </li>
                    <li>
                        <a href="#">Chart</a>
                    </li>
                    <li>
                        <a href="#">Statistical</a>
                    </li>
                </ul>
            </li>
        <div class="clearfix"></div>

        </ul>
    <!-- Sidebar -left -->

</div>

