<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                2022 @ Nhóm 9 Kiến Trúc Phần Mềm
            </div>
            <div class="col-md-6">
                <div class="text-md-right footer-links d-none d-md-block">
                    <a href="#">About</a>
                    <a href="#">Support</a>
                    <a href="#">Contact Us</a>s
                </div>
            </div>
        </div>
    </div>
</footer>
