<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    <link rel="stylesheet" href="{{ URL::asset('css/style-login.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
</head>
<body>
<div class="header">
    <h2>Register</h2>
</div>
<form action="{{route('registration')}}" method="post" >
    {{ csrf_field() }}
    <div class="input-group">
        <label>Email</label>
        <input type="text" name="email" required>
    </div>

    <div class="input-group">
        <label>Username</label>
        <input type="text" name="username" required>
    </div>

    <div class="input-group">
        <label>Password</label>
        <input type="password" name="password" required>
    </div>

    <div class="input-group">
        <label>Confirm Password</label>
        <input type="password" name="confirm_password"required>
    </div>

    <div class="input-group">
        <button type="submit" class="btn" >Register</button>
    </div>
</form>
</body>
</html>
@if(session('success'))
    {{session('success')}}
@endif
